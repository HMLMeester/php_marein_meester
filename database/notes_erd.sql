-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 08 jun 2017 om 14:15
-- Serverversie: 10.1.16-MariaDB
-- PHP-versie: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notes_erd`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `notes`
--

CREATE TABLE `notes` (
  `NoteID` int(11) NOT NULL,
  `authorID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `Titel` varchar(255) NOT NULL,
  `Tekst` varchar(255) NOT NULL,
  `ReadingRights` int(11) NOT NULL,
  `TimeWrote` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TimeChanged` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `notes`
--

INSERT INTO `notes` (`NoteID`, `authorID`, `studentID`, `Titel`, `Tekst`, `ReadingRights`, `TimeWrote`, `TimeChanged`) VALUES
(1, 5, 3, 'rnjasgdfah', 'dgilahfskjvbdsfg erhg fbfrfb gh fmasd gbfj bgjdsf gdsf b,jds,gj ', 1, '2017-06-02 00:00:00', NULL),
(2, 5, 3, 'hallo?', 'nou je doet het goed hoor superb.', 0, '2017-06-03 11:40:00', NULL),
(3, 5, 3, 'jajajaja', 'neeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneeneenee', 0, '2017-06-03 08:25:32', NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `permissions`
--

CREATE TABLE `permissions` (
  `RoleID` int(11) NOT NULL,
  `Role` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `permissions`
--

INSERT INTO `permissions` (`RoleID`, `Role`) VALUES
(0, 'student'),
(1, 'docent'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `readingrights`
--

CREATE TABLE `readingrights` (
  `ReadingRightsID` int(11) NOT NULL,
  `ReadingRights` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `readingrights`
--

INSERT INTO `readingrights` (`ReadingRightsID`, `ReadingRights`) VALUES
(0, 'mag dit lezen'),
(1, 'mag dit niet lezen');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `Username` varchar(45) NOT NULL,
  `Password` varchar(45) NOT NULL,
  `DOB` date NOT NULL,
  `mentorID` int(11) DEFAULT NULL,
  `Role` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Surname` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`UserID`, `Username`, `Password`, `DOB`, `mentorID`, `Role`, `Name`, `Surname`) VALUES
(3, 'marein', 'root', '2000-03-14', 5, 0, 'marein', 'meester'),
(5, 'docent', 'root', '1970-03-24', NULL, 1, 'bennie', 'waalstra'),
(8, 'ReflexZ', 'root', '2000-08-18', NULL, 0, 'piet', 'paulusma');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`NoteID`),
  ADD KEY `Student_idx` (`studentID`),
  ADD KEY `readingrights_idx` (`ReadingRights`),
  ADD KEY `docent` (`authorID`);

--
-- Indexen voor tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`RoleID`);

--
-- Indexen voor tabel `readingrights`
--
ALTER TABLE `readingrights`
  ADD PRIMARY KEY (`ReadingRightsID`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserID`),
  ADD KEY `permission_idx` (`Role`),
  ADD KEY `mentor_idx` (`mentorID`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `notes`
--
ALTER TABLE `notes`
  MODIFY `NoteID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `Student` FOREIGN KEY (`studentID`) REFERENCES `users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `docent` FOREIGN KEY (`authorID`) REFERENCES `users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `readingrights` FOREIGN KEY (`ReadingRights`) REFERENCES `readingrights` (`ReadingRightsID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Beperkingen voor tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `mentor` FOREIGN KEY (`mentorID`) REFERENCES `users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `permission` FOREIGN KEY (`Role`) REFERENCES `permissions` (`RoleID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
