<?php

session_start();

function getFromSession($name, $default) {
    if (isset($_SESSION[$name])) {
        if (is_array($_SESSION[$name])) {
            $item = array_merge($default, $_SESSION[$name]);
        } else {
            $item = $_SESSION[$name];
        }
        unset($_SESSION[$name]);
    } else {
        $item = $default;
    }
    return $item;
}
    

