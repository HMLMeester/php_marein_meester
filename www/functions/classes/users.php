<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 13-6-2017
 * Time: 09:23
 */

class users{

    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    //check login data.
    function checkUser($username, $password, $redirectNeg, $redirectSTU, $redirectTEA, $redirectADM){
        //query
        $query = 'SELECT Username,Password,UserID,Name,Surname,DOB,role
                  FROM users 
                  WHERE Username=:username';
        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':username', $username, PDO::PARAM_STR);
        $ok = $statement->execute();

        //check if there aren't any errors
        if (!$ok) {
            $info = $statement->errorInfo();
            die('SQL-error: ' . $info[2]);
        } else {

            //succes now check for correct login data
            $users = $statement->fetch(PDO::FETCH_ASSOC);
            if (empty($users)) {
                $_SESSION['username']    = 'onjuiste username.';
                header('location: ' . $redirectNeg);
            } else if ($users['Password'] != $password) {
                $_SESSION['password']    = 'onjuist wachtwoord.';
                header('location: ' . $redirectNeg);
            } else if ($users['Password'] === $password) {

                //give positive error
                $_SESSION['logged']    = 'succesvol ingelogt';

                //get name and userID in the session
                $_SESSION['userID']     = $users['UserID'];
                $_SESSION['name']       = $users['Name'];
                $_SESSION['surname']    = $users['Surname'];
                $_SESSION['DOB']        = $users['DOB'];
                $_SESSION['Role']       = $users['role'];

                //0 = student
                //1 = teacher
                //2 = admin
                //check the role
                if($users['role'] == 0){

                    header('location: ' . $redirectSTU);

                } else if($users['role'] == 1){

                    header('location: ' . $redirectTEA);

                } else if($users['role'] == 2){

                    header('location: ' . $redirectADM);

                }

            }
        }
    }

    function getStudents(){

        $query = 'SELECT Name,Surname,DOB,mentorID,userID
                  FROM users
                  WHERE Role=0';
        $statement = $this->pdo->prepare($query);
        $ok = $statement->execute();
        if (!$ok) {

            $info = $statement->errorInfo();
            die('SQL-error: ' . $info[2]);

        } else {

            return $statement->fetchAll(PDO::FETCH_ASSOC);

        }
    }

    function getMentorStudent($userID){

        $query = 'SELECT Name,Surname,DOB,mentorID
                  FROM users
                  WHERE Role=0 AND mentorID=:userID';
        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':userID', $userID, PDO::PARAM_STR);
        $ok = $statement->execute();
        if (!$ok) {

            $info = $statement->errorInfo();
            die('SQL-error: ' . $info[2]);

        } else {

            return $statement->fetchAll(PDO::FETCH_ASSOC);

        }

    }

    function getAllUsers(){

        $query = "SELECT s.UserID,s.Name,s.Surname,s.DOB,s.Username, 
                         CONCAT(mentor.Name ,' ', mentor.Surname) AS mentorName
                         ,p.Role AS userRole
                  FROM users s
                  JOIN permissions AS p     ON p.roleID=s.Role
                  LEFT JOIN users AS mentor ON s.mentorID=mentor.UserID";
        $statement = $this->pdo->prepare($query);
        $ok = $statement->execute();
        if (!$ok) {

            $info = $statement->errorInfo();
            die('SQL-error: ' . $info[2]);

        } else {

            return $statement->fetchAll(PDO::FETCH_ASSOC);

        }

    }

    function getMentors(){

        $query = 'SELECT Name, Surname, UserID
                  FROM users
                  WHERE Role = 1 OR Role = 2';
        $statement = $this->pdo->prepare($query);
        $ok = $statement->execute();
        if (!$ok) {

            $info = $statement->errorInfo();
            die('SQL-error: ' . $info[2]);

        } else {

            return $statement->fetchAll(PDO::FETCH_ASSOC);

        }

    }

    function delUser($userID){

        $query2 = 'DELETE FROM users WHERE UserID=:ID';
        $statement2 = $this->pdo->prepare($query2);
        $statement2->bindValue(':ID', $userID, PDO::PARAM_INT);
        $ok2 = $statement2->execute();

        if (!$ok2){

            $query = 'DELETE FROM notes WHERE authorID=:ID OR studentID=:ID';
            $statement = $this->pdo->prepare($query);
            $statement->bindValue(':ID', $userID, PDO::PARAM_INT);
            $ok1 = $statement->execute();


        }

        return $ok2;

    }

    function getUser($userID){

        $query = 'SELECT u.Name, u.Surname, u.Role as RoleID, u.UserID, permissions.Role
                  FROM users u
                  JOIN permissions ON u.Role=permissions.RoleID
                  WHERE UserID=:id';
        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':id', $userID, PDO::PARAM_INT);
        $ok = $statement->execute();
        if (!$ok) {

            return $statement->errorInfo();

        } else {

            return $statement->fetch(PDO::FETCH_ASSOC);

        }

    }

    function updateUser($role, $name, $surname, $userID){

        $query = 'UPDATE users SET Role=:role,Name=:name,Surname=:surname WHERE UserID=:id';
        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':role', $role, PDO::PARAM_INT);
        $statement->bindValue(':name', $name, PDO::PARAM_STR);
        $statement->bindValue(':surname', $surname, PDO::PARAM_STR);
        $statement->bindValue(':id', $userID, PDO::PARAM_INT);
        return $statement->execute();
        if (!$ok) {
            $info = $statement->errorInfo();
            die($info[2]);
        }

    }

}