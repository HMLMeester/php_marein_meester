<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 13-6-2017
 * Time: 09:15
 */
class notes{

    private $pdo;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    function getAllNotesStudent($userID){

        $query = '
        SELECT n.Titel,n.Tekst,n.TimeWrote,n.TimeChanged,n.StudentID,n.ReadingRights,n.authorID,
               users.UserID,users.Name,users.Surname
        FROM notes n
        JOIN users ON users.UserID=n.authorID
        WHERE n.studentID=:ID AND n.readingRights=0
        ORDER BY TimeWrote DESC';

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':ID', $userID, PDO::PARAM_INT);
        $ok = $statement->execute();
        if (!$ok) {
            $info = $statement->errorInfo();
            die('SQL-error: ' . $info[2]);
        } else {
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

    }

    function getAllnotes(){
        $query = '
        SELECT n.NoteID,n.tekst,n.TimeChanged,n.TimeWrote,n.Titel,n.authorID,
               readingrights.ReadingRights,
               student.Name,student.Surname,
               author.Name AS Aname,author.Surname AS Asurname
        FROM notes n
        JOIN ReadingRights ON readingrights.ReadingRightsID=n.ReadingRights
        JOIN users AS student   ON student.UserID=StudentID
        JOIN users AS author	ON author.UserID=n.authorID
        ORDER BY TimeWrote DESC';
        $statement = $this->pdo->prepare($query);
        $ok = $statement->execute();
        if (!$ok) {
            $info = $statement->errorInfo();
            die('SQL-error: ' . $info[2]);
        } else {
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    function addNote($title, $text, $author, $student, $read){
        $query = 'INSERT INTO notes (Titel,Tekst,authorID,studentID,ReadingRights) 
                  VALUES (:title,:text,:author,:student,:readR)';
        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':title', $title, PDO::PARAM_STR);
        $statement->bindValue(':text', $text, PDO::PARAM_STR);
        $statement->bindValue(':author', $author, PDO::PARAM_INT);
        $statement->bindValue(':student', $student, PDO::PARAM_INT);
        $statement->bindValue(':readR', $read, PDO::PARAM_INT);
        $ok = $statement->execute();

        return $ok;
    }

    function deleteNote($noteID){

        $query = 'DELETE FROM notes WHERE NoteID=:noteID';
        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':noteID', $noteID, PDO::PARAM_INT);
        return $statement->execute();

    }

    function getNoteValues($noteID){

        $query = '
                  SELECT n.Titel,n.Tekst,n.ReadingRights,n.StudentID,users.Name,users.Surname,readingrights.ReadingRights
                  FROM notes n
                  JOIN users ON n.StudentID=users.UserID
                  JOIN readingrights ON n.ReadingRights=readingrights.ReadingRightsID
                  WHERE noteID=:noteID';
        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':noteID', $noteID, PDO::PARAM_STR);
        $ok = $statement->execute();
        if (!$ok) {

            $info = $statement->errorInfo();
            die('SQL-error: ' . $info[2]);

        } else {

            return $statement->fetch(PDO::FETCH_ASSOC);

        }

    }

    function changeNote($title, $text, $student, $read, $noteID){
        $query = 'UPDATE notes SET titel=:titel,tekst=:tekst,studentID=:student,ReadingRights=:readR WHERE noteID=:id';
        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':titel', $title, PDO::PARAM_STR);
        $statement->bindValue(':tekst', $text, PDO::PARAM_STR);
        $statement->bindValue(':student', $student, PDO::PARAM_INT);
        $statement->bindValue(':readR', $read, PDO::PARAM_INT);
        $statement->bindValue(':id', $noteID, PDO::PARAM_INT);
        $ok = $statement->execute();
        if (!$ok) {
            $info = $statement->errorInfo();
            die($info[2]);
        } else {
            return $ok;
        }
    }

}