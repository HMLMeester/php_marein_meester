<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 2-6-2017
 * Time: 09:16
 */
class DataBase{

    function db($username, $password){
        $driver = 'mysql';
        $host = '127.0.0.1';
        $port = '3306';
        $dbname = 'notes_erd';
        $dsn = $driver . ':host=' . $host . ';port=' . $port . ';dbname=' . $dbname;

        $pdo = new PDO($dsn, $username, $password);

        return $pdo;
    }
}