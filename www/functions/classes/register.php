<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 2-6-2017
 * Time: 11:29
 */
class register{

    private $pdo;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    function checkUsername($username){
        $query = 'SELECT Username
                  FROM users
                  WHERE Username=:username';
        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':username', $username, PDO::PARAM_STR);
        $ok = $statement->execute();
        if (!$ok) {
            $info = $statement->errorInfo();
            die('SQL-error: ' . $info[2]);
        } else {
            $users = $statement->fetch(PDO::FETCH_ASSOC);

            if(!empty($users)){
                return false;
            } else {
                return true;
            }
        }
    }

    function addUser($username, $password, $DOB, $name, $surname, $role){
        $query = 'INSERT INTO users (Username,Password,DOB,name,surname,Role) 
                  VALUES (:username,:password,:DOB,:fname,:surname,:role)';
        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':username', $username, PDO::PARAM_STR);
        $statement->bindValue(':password', $password, PDO::PARAM_STR);
        $statement->bindValue(':DOB', $DOB, PDO::PARAM_STR);
        $statement->bindValue(':fname', $name, PDO::PARAM_STR);
        $statement->bindValue(':surname', $surname, PDO::PARAM_STR);
        $statement->bindValue(':role', $role, PDO::PARAM_INT);
        $ok = $statement->execute();
        if (!$ok) {
            $info = $statement->errorInfo();
            die('SQL-error: ' . $info[2]);
        }

        return $ok;
    }

    function getRoles(){

        $query = 'SELECT *
                  FROM permissions';
        $statement = $this->pdo->prepare($query);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);


    }
}