<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 12-6-2017
 * Time: 11:04
 */
class changeMentor{

    private $pdo;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    function changeMentor($userID, $mentorID){

        $query = 'UPDATE users SET mentorID=:mentorID WHERE UserID=:id';
        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':id', $userID, PDO::PARAM_INT);
        $statement->bindValue(':mentorID', $mentorID, PDO::PARAM_INT);
        return $statement->execute();


    }

}