<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 2-6-2017
 * Time: 09:17
 */
spl_autoload_register(function ($class_name) {
    include 'classes/' . $class_name . '.php';
});