<?php

require_once('../functions/session.php');
require_once('../functions/autoloader.php');

$redirect = new redirect();

//get information from session
$usernameError = getFromSession('username', '');
$passwordError = getFromSession('password', '');
$registerMES   = getFromSession('register', '');
$noLogin       = getFromSession('noLogin', '');
$logout        = getFromSession('logout', '');

$errorMessages = new errorMessages();

if(isset($_SESSION['userID'])){

    if($_SESSION['Role'] == 0){
        $redirect->redirecter('studentNotes.php', '', '');
    } elseif($_SESSION['Role'] == 1){
        $redirect->redirecter('teacher/teacherNotes.php', '', '');
    } elseif($_SESSION['Role'] == 2){
        $redirect->redirecter('admin/admin.php', '', '');
    }

}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Welkom</h1>
<?php
$errorMessages->errorMes($registerMES);
$errorMessages->errorMes($noLogin);
$errorMessages->errorMes($logout);
?>
<form action="check.php" method="POST">
    <fieldset>

        <legend> Log in: </legend>
        <p>Username</p>
        <input type="text" name="username" placeholder="Username">
        <?php
        $errorMessages->errorMes($usernameError);
        ?>
        <p>Wachtwoord</p>
        <input type="password" name="password" placeholder="Password">
        <?php
        $errorMessages->errorMes($passwordError);
        ?>
        <br><br><input type="submit" value="log in!">
    </fieldset>
</form>
</body>
</html>
