<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 2-6-2017
 * Time: 09:15
 */

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();


if(empty($_SESSION['userID'])){
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 1 ){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} else {

    //get info out of session
    $requestError = getFromSession('noPost', '');
    $titleError = getFromSession('title', '');
    $textError = getFromSession('tekst', '');
    $studentError = getFromSession('student', '');
    $readError = getFromSession('read', '');
    $DBERROR = getFromSession('ERRORDB', '');
    $succes = getFromSession('succesChange','');

    //get information out of the session
    $userID = $_SESSION['userID'];
    $name = $_SESSION['name'];
    $userSurname = $_SESSION['surname'];
    $userDOB = $_SESSION['DOB'];
    $userRole = $_SESSION['Role'];

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    //get the noteID
    $noteID = $_GET['noteID'];

    //classes
    $database = new database();
    $pdo = $database->db('docent', 'root');
    $getUsers = new users($pdo);
    $errorMessages = new errorMessages();
    $getNoteValues = new notes($pdo);

    //get all students
    $users = $getUsers->getStudents();
    $noteValues = $getNoteValues->getNoteValues($noteID);

    $_SESSION['Title'] = $noteValues['Titel'];
    $_SESSION['Text'] = $noteValues['Tekst'];
    $_SESSION['Student'] = $noteValues['StudentID'];
    $_SESSION['Read'] = $noteValues['ReadingRights'];
    $_SESSION['note'] = $noteID;

    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../style/styles.css">
        <title>Document</title>
    </head>
    <body>
    <h2>Welkom <?php echo $name; ?>.</h2>
    <p><a href="teacherNotes.php">ga terug.</a></p>
    <?php
    $errorMessages->errorMes($requestError);
    $errorMessages->errorMes($titleError);
    $errorMessages->errorMes($textError);
    $errorMessages->errorMes($studentError);
    $errorMessages->errorMes($readError);
    $errorMessages->errorMes($DBERROR);
    $errorMessages->errorMes($succes);

    ?>
    <form action="changeN.php?noteID=<?php echo $noteID ?>" method="POST">
        <fieldset>

            <legend> notities: </legend>
            <p>Titel</p>
            <input type="text" name="title" value="<?php echo $noteValues['Titel'] ?>">
            <p>tekst</p>
            <textarea name='text' rows="4" cols="50" id="textArea" maxlength="255"><?php echo $noteValues['Tekst'] ?></textarea>
            <p><small id="charRem"></small></p>
            <p>over wie gaat het?</p>
            <label>
                <select name="Student">
                    <option value="<?php echo $noteValues['StudentID'] ?>"><?php echo $noteValues['Name'] . ' ' . $noteValues['Surname'] ?></option>
                    <?php foreach ($users as $user){

                        if($user['userID'] === $noteValues['StudentID']){
                            unset($user['userID']);
                        } else {

                        ?>

                    <option value="<?php echo $user['userID'] ?>"><?php echo $user['Name'] . ' ' . $user['Surname'] ?></option>

                    <?php }
                    } ?>
                </select>
            </label>
            <p>mag de student dit lezen?</p>
            <label>
                <select name="read">
                    <?php if($noteValues['ReadingRights'] === 1){ ?>
                    <option value="1">nee</option>
                    <option value="0">Ja</option>
                    <?php } else { ?>
                    <option value="0">ja</option>
                    <option value="1">nee</option>
                    <?php } ?>
                </select>
            </label>
            <br><br><input type="submit" value="Verzenden">
        </fieldset>
    </form>

    </body>
    <script src="../JS/main.js"></script>
    </html>
    <?php
}
?>