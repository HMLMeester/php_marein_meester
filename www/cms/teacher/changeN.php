<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 8-6-2017
 * Time: 11:09
 */

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();

//validation
if(empty($_SESSION['userID'])){
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 1 ){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} elseif(empty($_POST)){
    //illegal request method.
    $redirect->redirecter('addnote.php', 'noPost', 'Ongeldige request methode.');
} else {

    //get information out of the session
    $userID = $_SESSION['userID'];
    $name = $_SESSION['name'];
    $userSurname = $_SESSION['surname'];
    $userDOB = $_SESSION['DOB'];
    $userRole = $_SESSION['Role'];

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    //post is correct
    //get everything out of the post.
    $title          = $_POST['title'];
    $text           = $_POST['text'];
    $student        = $_POST['Student'];
    $readable       = $_POST['read'];
    $noteID         = $_GET['noteID'];

    //get old values
    $_SESSION['Title'];
    $_SESSION['Text'];
    $_SESSION['Student'];
    $_SESSION['Read'];
    $_SESSION['note'];


    //max characters for strings
    $maxLength      = 255;

    $titleLength    = strlen($title);
    $textLength     = strlen($text);


    if($titleLength > $maxLength){
        $redirect->redirecter('changenote.php?noteID=' . $noteID, 'title', 'Uw titel is te lang.');
    } elseif($textLength > $maxLength){
        $redirect->redirecter('changenote.php?noteID=' . $noteID, 'tekst', 'Uw tekst is te lang.');
    } elseif($student === 0){
        $redirect->redirecter('changenote.php?noteID=' . $noteID, 'student', 'geen student ingevuld.');
    } elseif($readable == '#'){
        $redirect->redirecter('changenote.php?noteID=' . $noteID, 'read', 'niet ingevuld of de student het mag lezen');
    } else {

        //classes
        $database = new database();
        $pdo = $database->db('docent', 'root');
        $changeNote = new notes($pdo);

        //adds the values in the database
        $ok = $changeNote->changeNote($title, $text, $student, $readable, $noteID);

        if(!$ok){
            $redirect->redirecter('changenote.php?noteID=' . $noteID, 'succesChange', 'er is iets fout gegaan');
        } else {
            $redirect->redirecter('teacherNotes.php', 'succesAdd', 'Notitie is veranderd.');
        }

    }

}