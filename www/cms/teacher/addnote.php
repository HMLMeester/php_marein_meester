<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 2-6-2017
 * Time: 09:15
 */

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();


if(empty($_SESSION['userID'])){
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 1 ){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} else {

    //get info out of session
    $requestError = getFromSession('noPost', '');
    $titleError = getFromSession('title', '');
    $textError = getFromSession('tekst', '');
    $studentError = getFromSession('student', '');
    $readError = getFromSession('read', '');
    $DBERROR = getFromSession('ERRORDB', '');

    //get information out of the session
    $userID = $_SESSION['userID'];
    $name = $_SESSION['name'];
    $userSurname = $_SESSION['surname'];
    $userDOB = $_SESSION['DOB'];
    $userRole = $_SESSION['Role'];

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    //classes
    $database = new database();
    $pdo = $database->db('docent', 'root');
    $getUsers = new users($pdo);
    $errorMessages = new errorMessages();

    //get all students
    $users = $getUsers->getStudents();

    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../style/styles.css">
        <title>Document</title>
    </head>
    <body>
    <header>
        <ul>
            <li><a href="students.php">Alle studenten</a></li>
            <li><a href="teacherNotes.php">Alle notities</a></li>
            <li><a href="../logout.php">log uit</a></li>
        </ul>
    </header>
    <h2>Welkom <?php echo $name; ?>.</h2>
    <p><a href="teacherNotes.php">ga terug.</a></p>
    <?php
    $errorMessages->errorMes($requestError);
    $errorMessages->errorMes($titleError);
    $errorMessages->errorMes($textError);
    $errorMessages->errorMes($studentError);
    $errorMessages->errorMes($readError);
    $errorMessages->errorMes($DBERROR);
    ?>
    <form action="makeNote.php" method="POST">
        <fieldset>

            <legend> notities: </legend>
            <p>Titel</p>
            <input type="text" name="title" placeholder="titel">
            <p>tekst</p>
            <textarea name='text' rows="4" cols="50" value="tekst" id="textArea" maxlength="255"></textarea>
            <p><small id="charRem">je hebt nog 255 karakters over</small></p>
            <p>over wie gaat het?</p>
            <label>
                <select class='select' name="Student">
                    <option value="0">kies een leerling</option>
                    <?php foreach ($users as $user){ ?>

                    <option value="<?php echo $user['userID'] ?>"><?php echo $user['Name'] . ' ' . $user['Surname'] ?></option>

                    <?php } ?>
                </select>
            </label>
            <p>mag de student dit lezen?</p>
            <label>
                <select name="read">
                    <option value="#">vul in.</option>
                    <option value="0">Ja</option>
                    <option value="1">Nee</option>
                </select>
            </label>
            <br><br><input class='submit' type="submit" value="Verzenden">
        </fieldset>
    </form>

    </body>
    <script src="../JS/main.js"></script>
    </html>
    <?php
}
?>