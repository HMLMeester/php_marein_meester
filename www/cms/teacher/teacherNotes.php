<?php

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();

//if the userID session var is empty
if(empty($_SESSION['userID'])){
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 1){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} else {

    //get information out of the session
    $userID = $_SESSION['userID'];
    $name = $_SESSION['name'];
    $userSurname = $_SESSION['surname'];
    $userDOB = $_SESSION['DOB'];
    $userRole = $_SESSION['Role'];

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    $succesAdd = getFromSession('succesAdd', '');
    $delErrorReq = getFromSession('delErrorReq', '');
    $delSucces = getFromSession('delSucces', '');

    //classes
    $database = new database();
    $pdo = $database->db('docent', 'root');
    $errorMessages = new errorMessages();
    $getNotes = new notes($pdo);

    //get the notes for the student
    $notes = $getNotes->getAllnotes();

    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../style/styles.css">
        <title>Document</title>
    </head>
    <body>
    <header>
        <ul>
            <li><a href="students.php">Alle studenten</a></li>
            <li><a href="addnote.php">notitie toevoegen</a></li>
            <li><a href="../logout.php">log uit</a></li>
        </ul>
    </header>
    <h2>Welkom <?php echo $name; ?>.</h2>
    <?php

    $errorMessages->errorMes($succesAdd);
    $errorMessages->errorMes($delErrorReq);
    $errorMessages->errorMes($delSucces);

    ?>

<!--notes-->
    <?php
    foreach ($notes as $note) { ?>
        <fieldset>
        <h2><?php echo $note['Titel']; ?></h2>
        <p><?php echo $note['tekst']; ?></p>
        <p>
            <small>geschreven: <?php echo $note['TimeWrote']; ?></small>
        </p>
        <?php if (isset($note['TimeChanged'])) { ?>

            <p>
                <small>veranderd:<?php echo $note['TimeChanged'] ?></small>
            </p>

        <?php } ?>
        <p>
            <small><?php echo $note['Name'] . ' ' . $note['Surname'] . ' ' . $note['ReadingRights'] . '.'; ?></small>
        </p>
        <p>
            <small> geschreven door <?php echo $note['Aname'] . ' ' . $note['Asurname']; ?></small>
        </p>
        <?php if ($note['authorID'] === $userID) { ?>
            <p>
                <small><a href="changenote.php?noteID=<?php echo $note['NoteID']; ?>">Edit</a></small>
                <small><a href="delete.php?noteID=<?php echo $note['NoteID']; ?>">Delete</a></small>
            </p>
            <?php
        }
        ?>
        </fieldset>
<?php
    }
    ?>

    </body>
    </html>
    <?php

}

?>