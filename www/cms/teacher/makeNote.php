<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 8-6-2017
 * Time: 11:09
 */

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();
//validation
if(empty($_SESSION['userID'])){
    //not logged in
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 1 ){
    //no permission for this page.
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} elseif(empty($_POST)){
    //illegal request method.
    $redirect->redirecter('addnote.php', 'noPost', 'Ongeldige request methode.');
} else {

    //get information out of the session
    $userID = $_SESSION['userID'];
    $name = $_SESSION['name'];
    $userSurname = $_SESSION['surname'];
    $userDOB = $_SESSION['DOB'];
    $userRole = $_SESSION['Role'];

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    //post is correct
    //get everything out of the post.
    $title          = $_POST['title'];
    $text           = $_POST['text'];
    $student        = $_POST['Student'];
    $readable       = $_POST['read'];
    $author         = $_SESSION['userID'];

    //max characters for strings
    $maxLength      = 255;

    $titleLength    = strlen($title);
    $textLength     = strlen($text);


    if($titleLength > $maxLength){
        $redirect->redirecter('addnote.php','title','Uw titel is te lang.');
    } elseif($textLength > $maxLength){
        $redirect->redirecter('addnote.php','tekst','Uw tekst is te lang.');
    } elseif($student === 0){
        $redirect->redirecter('addnote.php','student','geen student ingevuld.');
    } elseif($readable == '#'){
        $redirect->redirecter('addnote.php','read','niet ingevuld of de student het mag lezen');
    } else {

        //classes
        $database = new database();
        $pdo = $database->db('docent', 'root');
        $addNote = new notes($pdo);

        //adds the values in the database
        $ok = $addNote->addNote($title, $text, $author, $student, $readable);

        if(!$ok){
            $redirect->redirecter('addnote.php','ERRORDB','er is iets foet gegaan bij het maken van de note.');
        } else {
            $redirect->redirecter('teacherNotes.php','succesAdd','Notitie is toegevoegd.');
        }

    }

}