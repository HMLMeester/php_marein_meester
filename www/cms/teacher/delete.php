<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 8-6-2017
 * Time: 14:35
 */

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();

if(empty($_SESSION['userID'])){
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 1 ){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} else {

    //get information out of the session
    $userID = $_SESSION['userID'];
    $name = $_SESSION['name'];
    $userSurname = $_SESSION['surname'];
    $userDOB = $_SESSION['DOB'];
    $userRole = $_SESSION['Role'];

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    //classes
    $database = new database();
    $pdo = $database->db('docent', 'root');
    $delNote = new notes($pdo);

    if (empty($_GET)){

        //if there is no GET request
        $redirect->redirecter('teacherNotes.php', 'delErrorReq', 'Ongeldige request methode.');

    } else {

        //get the noteID out of the GET
        $noteID = $_GET['noteID'];

        //deletes the note
        $ok = $delNote->deleteNote($noteID);

        if(!$ok){

            $redirect->redirecter('teacherNotes.php', 'delError', 'er is iets mis gegaan bij het verwijderen.');

        } else {

            $redirect->redirecter('teacherNotes.php', 'delSucces', 'Verwijderen is gelukt.');

        }

    }

}
