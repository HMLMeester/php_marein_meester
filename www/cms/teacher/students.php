<?php
require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();

if(empty($_SESSION['userID'])){
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 1 ){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} else {

    //get information out of the session
    $userID = $_SESSION['userID'];
    $name = $_SESSION['name'];
    $userSurname = $_SESSION['surname'];
    $userDOB = $_SESSION['DOB'];
    $userRole = $_SESSION['Role'];

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    //classes
    $database = new database();
    $pdo = $database->db('docent', 'root');

    $getStudent = new users($pdo);

    //get all the students
    $users = $getStudent->getStudents();

    //get all mentor students.
    $mentorUsers = $getStudent->getMentorStudent($userID);

    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../style/styles.css">
        <title>Document</title>
    </head>
    <body>
    <header>
        <ul>
            <li><a href="teacherNotes.php">Notities</a></li>
            <li><a href="addnote.php">notitie toevoegen</a></li>
            <li><a href="../logout.php">log uit</a></li>
        </ul>
    </header>
    <table>
        <h1>Alle studenten.</h1>
        <thead>
        <tr>
            <th>Naam</th>
            <th>Achternaam</th>
            <th>Geboorte Datum</th>
        </tr>
        </thead>
        <tbody>
    <?php
    foreach ($users as $user) { ?>
        <tr>

            <td><?php echo $user['Name']; ?></td>
            <td><?php echo $user['Surname']; ?></td>
            <td><?php echo $user['DOB']; ?></td>

        </tr>
        <?php
    }

    ?>
        </tbody>
    </table>
    <br>
    <?php if($mentorUsers){?>

        <table>

            <h1>Mentor Leerlingen.</h1>

            <thead>
            <tr>
                <th>Naam</th>
                <th>Achternaam</th>
                <th>Geboorte Datum</th>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($mentorUsers as $mentorUser) { ?>
                <tr>

                    <td><?php echo $mentorUser['Name']; ?></td>
                    <td><?php echo $mentorUser['Surname']; ?></td>
                    <td><?php echo $mentorUser['DOB']; ?></td>

                </tr>
                <?php } ?>
            </tbody>


        </table>

    <?php } ?>
    </body>
    </html>
    <?php
}
?>