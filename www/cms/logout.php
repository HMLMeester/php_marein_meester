<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 3-6-2017
 * Time: 14:03
 */

require_once('../functions/session.php');
require_once('../functions/autoloader.php');

//classes
$redirect = new redirect();

//unset the session variables.
unset($_SESSION['userID']);
unset($_SESSION['name']);
unset($_SESSION['surname']);
unset($_SESSION['DOB']);
unset($_SESSION['Role']);

//redirect to index with session info.
$redirect->redirecter('index.php', 'logout', 'succesvol uitgelogt.');