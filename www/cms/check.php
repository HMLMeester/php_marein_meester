<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 2-6-2017
 * Time: 09:15
 */
require_once('../functions/session.php');
require_once('../functions/autoloader.php');

//classes
$database = new database();
$pdo = $database->db('admin', 'root');
$redirect = new redirect();
$checker = new users($pdo);

//check if the method is POST
if(empty($_POST)){
    $redirect->redirecter('index.php', 'message', 'ongeldige request methode');
} else {

    //get username and password
    $username = $_POST['username'];
    $password = $_POST['password'];

    //check if user exist and password is correct
    $checker->checkUser($username, $password, 'index.php', 'student/studentNotes.php', 'teacher/teacherNotes.php', 'admin/admin.php');

}