/**
 * Created by marei on 2-6-2017.
 */

const maxLength = 255;

document.querySelector('#textArea').addEventListener('keyup', function() {
    //get value
    const textAreaValue = document.querySelector('#textArea').value;
    let length = textAreaValue.length;

    length = maxLength - length;

    if(length === 0){

        document.querySelector('#charRem').innerHTML = 'er kunnen geen karakters meer bij.';

    } else if(length > maxLength){

        textAreaValue.disabled = true;

        document.querySelector('#charRem').innerHTML = 'er kunnen niet meer dan 255 karakters ingevoegd worden.';

    }else {

        document.querySelector('#charRem').innerHTML = 'je hebt nog ' + length + ' karakters over';


    }
});