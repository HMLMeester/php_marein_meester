<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 8-6-2017
 * Time: 11:09
 */

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();

//validation
if(empty($_SESSION['userID'])){
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 2 ){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} elseif(empty($_POST)){
    $redirect->redirecter('addnote.php', 'noPost', 'Ongeldige request methode.');
} else {

    //get information out of the session
    $ID             = $_SESSION['userID'];
    $name           = $_SESSION['name'];
    $userSurname    = $_SESSION['surname'];
    $userDOB        = $_SESSION['DOB'];
    $userRole       = $_SESSION['Role'];

    //post is correct
    //get everything out of the post.
    $namep          = $_POST['Name'];
    $surName        = $_POST['surname'];
    $Role           = $_POST['role'];
    $userID         = $_GET['userID'];


    if($namep === ''){
        $redirect->redirecter('updateUser.php', 'nameError', 'Geen naam ingevuld.');
    } elseif($surName === ''){
        $redirect->redirecter('updateUser.php', 'surNameError', 'Geen achternaam ingevuld');
    } else {

        //classes
        $database = new database();
        $pdo = $database->db('admin', 'root');
        $changeUser = new users($pdo);

        //adds the values in the database
        $ok = $changeUser->updateUser($Role, $namep, $surName, $userID);

        //if you change your own name it updates the session
        if($userID === $ID){

            $_SESSION['name'] = $namep;
            $_SESSION['surname'] = $surName;
            $_SESSION['Role'] = $Role;

        }


        if(!$ok){
            $redirect->redirecter('admin.php', 'errorUpdate', 'er is iets fout gegaan');
        } else {
            $redirect->redirecter('admin.php', 'succesUpdate', 'User is veranderd.');
        }

    }

}