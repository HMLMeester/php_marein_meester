<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 2-6-2017
 * Time: 09:14
 */

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();

if(empty($_SESSION['userID'])){
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 2 ){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} else {

    //get information out of the session
    $userID = $_SESSION['userID'];
    $name = $_SESSION['name'];
    $userSurname = $_SESSION['surname'];
    $userDOB = $_SESSION['DOB'];
    $userRole = $_SESSION['Role'];

    //get userID out of GET
    $ID = $_GET['id'];

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    $updateSucces = getFromSession('succesUpdate', '');
    $updateError = getFromSession('errorUpdate', '');
    $NameError = getFromSession('nameError', '');
    $surNameError = getFromSession('surNameError','');

    //classes
    $database = new database();
    $pdo = $database->db('admin', 'root');
    $errorMessages = new errorMessages();
    $getRoles = new register($pdo);
    $users = new users($pdo);

    //get the roles and user info
    $roles = $getRoles->getRoles();
    $userData = $users->getUser($ID);


    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../style/styles.css">
        <title>Document</title>
    </head>
    <body>
    <header>
        <ul>
            <li><a href="admin.php">alle users</a></li>
            <li><a href="../logout.php">log uit</a></li>
        </ul>
    </header>
    <h2>Welkom <?php echo $name; ?>.</h2>
    <h1>Student registreren.</h1>
    <?php

    $errorMessages->errorMes($updateSucces);
    $errorMessages->errorMes($updateError);
    $errorMessages->errorMes($NameError);
    $errorMessages->errorMes($surNameError);

    ?>
    <form action="changeUser.php?userID=<?php echo $userData['UserID'] ?>" method="POST">
        <fieldset>
            <legend> vul in: </legend>
            <p>voornaam</p>
            <input type="text" name="Name" value="<?php echo $userData['Name'] ?>">
            <p>achternaam</p>
            <input type="text" name="surname" value="<?php echo $userData['Surname'] ?>">
            <p>Rol</p>
            <label>
                <select class='select' name="role">
                    <option value="<?php echo $userData['RoleID'] ?>"><?php echo $userData['Role'] ?></option>
                    <?php foreach($roles as $role){

                        if($userData['RoleID'] == $role['RoleID']) {
                            unset($role['Role']);
                            unset($role['RoleID']);
                        } else {

                        ?>
                        <option value="<?php echo $role['RoleID'] ?>"><?php echo $role['Role'] ?></option>
                    <?php } } ?>
                </select>
            </label>
            <br><br><input class='submit' type="submit" value="update!">
        </fieldset>
    </form>

    </body>
    </html>
<?php } ?>