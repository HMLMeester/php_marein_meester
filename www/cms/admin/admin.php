<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 2-6-2017
 * Time: 11:19
 */

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();

if(empty($_SESSION['userID'])){
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 2 ){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} else {

    //get information out of the session
    $userID = $_SESSION['userID'];
    $name = $_SESSION['name'];
    $userSurname = $_SESSION['surname'];
    $userDOB = $_SESSION['DOB'];
    $userRole = $_SESSION['Role'];

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    //classes
    $database = new database();
    $pdo = $database->db('admin', 'root');
    $errorMessages = new errorMessages();
    $getUsers = new users($pdo);
    $getNotes = new notes($pdo);

    //get info out the session
    $menReqError = getFromSession('reqMen', '');
    $menNoChange = getFromSession('menNoChange', '');
    $noID = getFromSession('noUser', '');
    $noGET = getFromSession('noGet', '');
    $changeError = getFromSession('changeError', '');
    $changeSucces = getFromSession('changeSucces', '');
    $registerSucces = getFromSession('register', '');
    $DELErrorReq = getFromSession('delErrorReq', '');
    $DELError = getFromSession('delError', '');
    $DELSucces = getFromSession('delSucces', '');
    $updateSucces = getFromSession('succesUpdate', '');
    $updateError = getFromSession('errorUpdate', '');

    //get users and mentors.
    $users = $getUsers->getAllUsers();
    $mentors = $getUsers->getMentors();

    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../style/styles.css">
        <title>Document</title>
    </head>
    <body>
    <header>
        <ul>
            <li><a href="notes/adminNotes.php">Notities</a></li>
            <li><a href="notes/addnote.php">Notitie toevoegen</a></li>
            <li><a href="register.php">gebruiker toevoegen</a></li>
            <li><a href="../logout.php">log uit</a></li>
        </ul>
    </header>
    <h2>Welkom <?php echo $name; ?>.</h2>

    <?php

    //echo the error messeges from the session
    $errorMessages->errorMes($menReqError);
    $errorMessages->errorMes($menNoChange);
    $errorMessages->errorMes($noID);
    $errorMessages->errorMes($noGET);
    $errorMessages->errorMes($changeError);
    $errorMessages->errorMes($changeSucces);
    $errorMessages->errorMes($registerSucces);
    $errorMessages->errorMes($DELErrorReq);
    $errorMessages->errorMes($DELError);
    $errorMessages->errorMes($DELSucces);
    $errorMessages->errorMes($updateSucces);
    $errorMessages->errorMes($updateError);

    ?>
    <p>lijst met alle users.</p>
    <table>
        <thead>
        <tr>
            <th>UserID</th>
            <th>Voornaam</th>
            <th>Achternaam</th>
            <th>Geboortedatum</th>
            <th>UserName</th>
            <th>Rol</th>
            <th>Aantal notities</th>
            <th>Mentor</th>
            <th>Edit</th>
            <th>delete</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($users as $user) { ?>

            <tr>

                <td><?php echo $user['UserID']; ?></td>
                <td><?php echo $user['Name']; ?></td>
                <td><?php echo $user['Surname']; ?></td>
                <td><?php echo $user['DOB']; ?></td>
                <td><?php echo $user['Username']; ?></td>
                <td><?php echo ucfirst($user['userRole']); ?></td>
                <td><?php

                    $notes = $getNotes->getAllNotesStudent($user['UserID']);
                    if($user['userRole'] == 'student' and count($notes) > 0 ) {

                        ?>
                        <a href="notes/studentNotes.php?id=<?php echo $user['UserID']; ?>&user=<?php echo $user['Name'] ?>"><?php echo count($notes);?></a>
                        <?php

                    } else {

                        ?>
                        NVT
                        <?php

                    }
                    ?></td>

                <?php

                if($user['userRole'] === 'student') {

                    if ($user['mentorName'] === null) {
                        ?>

                        <td>
                            <form action="updateMentor.php?UserID=<?php echo $user['UserID']; ?>" method="POST">
                                <label>
                                    <select name="mentor">
                                        <option value="#">Geen mentor</option>
                                        <?php foreach ($mentors as $mentor) { ?>
                                            <option value="<?php echo $mentor['UserID'] ?>"><?php echo $mentor['Name'] . ' ' . $mentor['Surname'] ?></option>
                                        <?php } ?>
                                    </select>
                                </label>
                                <input type="submit" value="mentor veranderen.">
                            </form>
                        </td>

                    <?php
                    } else {
                        ?>

                        <td>
                            <form action="updateMentor.php?UserID=<?php echo $user['UserID']; ?>" method="POST">
                                <label>
                                    <select name="mentor">
                                        <option value="#"><?php echo $user['mentorName']; ?></option>
                                        <?php foreach ($mentors as $mentor) {

                                            $mentorNAME = $mentor['Name']. ' ' . $mentor['Surname'];

                                            if($mentorNAME == $user['mentorName']) {
                                                unset($mentor['Name']);
                                            } else {

                                            ?>
                                            <option value="<?php echo $mentor['UserID'] ?>"><?php echo $mentor['Name'] . ' ' . $mentor['Surname'] ?></option>

                                            <?php }
                                        }?>
                                        <option value="null">Geen mentor</option>
                                    </select>
                                </label>
                                <input type="submit" value="mentor veranderen.">
                            </form>
                        </td>

                    <?php }
                } else { ?>

                    <td>NVT</td>

                <?php } ?>

                <td><a href="updateUser.php?id=<?php echo $user['UserID']; ?>"><img src="../img/edit.svg" style="width:15px;height:15px;"</a> </td>
                <td><a href="delete.php?id=<?php echo $user['UserID']; ?>"><img src="../img/delete.svg" style="width:15px;height:15px;"></a></td>


            </tr>
            <?php
        }

        ?>
        </tbody>
    </table>


    <?php } ?>