<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 2-6-2017
 * Time: 09:14
 */

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();

if(empty($_SESSION['userID'])){
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 2 ){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} else {

    //get information out of the session
    $userID = $_SESSION['userID'];
    $name = $_SESSION['name'];
    $userSurname = $_SESSION['surname'];
    $userDOB = $_SESSION['DOB'];
    $userRole = $_SESSION['Role'];

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    $registerSucces = getFromSession('register', '');
    $roleError = getFromSession('roleError', '');

    //classes
    $database = new database();
    $pdo = $database->db('admin', 'root');
    $errorMessages = new errorMessages();
    $getRoles = new register($pdo);

    //get the roles
    $roles = $getRoles->getRoles();
?>
<!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../style/styles.css">
        <title>Document</title>
    </head>
    <body>
    <header>
        <ul>
            <li><a href="admin.php">Home</a></li>
            <li><a href="notes/adminNotes.php">Notities</a></li>
            <li><a href="notes/addnote.php">Notitie toevoegen</a></li>
            <li><a href="../logout.php">log uit</a></li>
        </ul>
    </header>
    <h2>Welkom <?php echo $name; ?>.</h2>
    <h1>Student registreren.</h1>
    <?php

    $errorMessages->errorMes($registerSucces);

    ?>
    <form action="addUser.php" method="POST">
        <fieldset>
            <legend> vul in: </legend>
            <p>Username</p>
            <input type="text" name="username" placeholder="Username">
            <p>Wachtwoord</p>
            <input type="password" name="password" placeholder="Password">
            <p>geboorte datum</p>
            <input type="date" name="DOB" placeholder="yyyy-mm-dd">
            <p>voornaam</p>
            <input type="text" name="name" placeholder="piet">
            <p>achternaam</p>
            <input type="text" name="surname" value="paulusma">
            <p>Rol</p>
            <label>
                <select class='select' name="role">
                    <option value="#">kies een rol.</option>
                    <?php foreach($roles as $role){ ?>
                        <option value="<?php echo $role['RoleID'] ?>"><?php echo $role['Role'] ?></option>
                    <?php } ?>
                </select>
                <?php $errorMessages->errorMes($roleError) ?>
            </label>
            <br><br><input class='submit' type="submit" value="Registreren!">
        </fieldset>
    </form>

    </body>
    </html>
<?php } ?>