<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 2-6-2017
 * Time: 11:17
 */

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

//classes
$database = new database();
$pdo = $database->db('admin', 'root');
$redirect = new redirect();
$check = new register($pdo);

//checks if the method is POST
if(empty($_POST)){
    $redirect->redirecter('../index.php', 'error', 'ongeldige request methode');
} elseif($_SESSION['Role'] != 2 ){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} else  {

    //get everything out the post
    $username = $_POST['username'];
    $password = $_POST['password'];
    $DOB = $_POST['DOB'];
    $name = $_POST['name'];
    $userSurname = $_POST['surname'];
    $role = $_POST['role'];

    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);


    if (!$check->checkUsername($username)) {

        //if the username already exist
        $_SESSION['username'] = 'username bestaat al.';

    } elseif($role < 0 or $role > 2){

        $_SESSION['roleError'] = 'Ongeldige rol';

    } else {

        //the user is added
        $ok = $check->addUser($username, $password, $DOB, $name, $userSurname, $role);

        //check if there aren't any errors
        if (!$ok) {

            $_SESSION['register'] = 'het registreren is niet gelukt.';

        } else {

            //everything is correct
            $redirect->redirecter('admin.php', 'register', 'de student is succesvol geregistreert.');

        }

    }

}