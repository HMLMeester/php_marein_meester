<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 12-6-2017
 * Time: 09:59
 */

require_once('../../functions/session.php');
require_once('../../functions/autoloader.php');

$redirect = new redirect();

if(empty($_SESSION['userID'])){
    $redirect->redirecter('../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 2 ){
    $redirect->redirecter('../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} elseif(empty($_POST)) {
    $redirect->redirecter('../admin.php','reqMen','Ongeldige request methode.');
} elseif(empty($_GET)) {
    $redirect->redirecter('admin.php','noGet','er is niks opgegeven.');
} else {

    $mentor = $_POST['mentor'];
    $user   = $_GET['UserID'];

    if($mentor === '#'){

        $redirect->redirecter('admin.php','menNoChange','mentor is niet veranderd.');

    } else {

        //classes
        $database = new database();
        $pdo = $database->db('admin', 'root');
        $errorMessages = new errorMessages();
        $getUsers = new users($pdo);
        $changeMentor = new changeMentor($pdo);


        if($mentor === 'null'){
            $mentor = null;
        }


            $ok = $changeMentor->changeMentor($user, $mentor);

            if(!$ok){
                $redirect->redirecter('admin.php','changeError','Er is iets mis gegaan tijdens het veranderen');
            } else {
                $redirect->redirecter('admin.php','changeSucces','mentor is veranderd');
            }


    }

}