<?php
/**
 * Created by PhpStorm.
 * User: marei
 * Date: 2-6-2017
 * Time: 14:20
 */

require_once('../../../functions/session.php');
require_once('../../../functions/autoloader.php');

$redirect = new redirect();

if(empty($_SESSION['userID'])){
    $redirect->redirecter('../../index.php', 'noLogin', 'er is niet ingelogt. log eerst in');
} elseif($_SESSION['Role'] != 2 ){
    $redirect->redirecter('../../index.php', 'noLogin', 'deze pagina is niet toegankelijk voor u.');
} else {

    //get information out of the session
    $userID = $_GET['id'];
    $studentName = $_GET['user'];
    $usID = $_SESSION['userID'];
    $name = $_SESSION['name'];
    $userSurname = $_SESSION['surname'];
    $userDOB = $_SESSION['DOB'];
    $userRole = $_SESSION['Role'];

    //make first character uppercase
    $name = ucfirst($name);
    $userSurname = ucfirst($userSurname);

    //classes
    $database = new database();
    $pdo = $database->db('student', 'root');
    $getNotes = new notes($pdo);

    //get the notes for the student
    $notes = $getNotes->getAllNotesStudent($userID);


    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../../style/styles.css">
        <title>Document</title>
    </head>
    <body>
    <header>
        <ul>
            <li><a href="../admin.php">Home</a></li>
            <li><a href="adminNotes.php">Notities</a></li>
            <li><a href="addnote.php">Notitie toevoegen</a></li>
            <li><a href="../register.php">gebruiker toevoegen</a></li>
            <li><a href="../../logout.php">log uit</a></li>
        </ul>
    </header>
    <h2>Welkom <?php echo $name; ?>.</h2>

    <p>alle notities over <?php echo $studentName ?></p>
    <?php
    if(empty($notes)){

        ?> <p> er zijn geen notities </p> <?php

    } else {
        foreach ($notes as $note) { ?>
            <fieldset>
                <h2><?php echo $note['Titel']; ?></h2>
                <p><?php echo $note['Tekst']; ?></p>
                <p>
                    <small><?php echo $note['TimeWrote'] ?></small>
                </p>
                <p>
                    <small>geschreven door <?php echo $note['Name'] . ' ' . $note['Surname'] ?></small>
                </p>
            </fieldset>
            <?php
        }
    }
    ?>
    </body>
    </html>
    <?php } ?>
